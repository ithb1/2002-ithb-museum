<?php
    $iduser = $this->session->userdata("id");
    $bulanSebelum = date("Y-m", strtotime("-1 month"));
?>

<style>
  .batasbawah{
    height: 400px !important;
  }
</style>

<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>


<section class="content">
    <?php
    foreach($rowTur as $row):
        if($row->status == "-1"):
            ?>
            <div class="alert alert-danger fade in m-b-15">
                Mohon maaf, museum tidak dapat melayani tur dalam pada tanggal <strong><?=date("d-m-Y", strtotime($row->tanggal))?></strong> jam <strong><?=date("H:i", strtotime($row->jam))?></strong> dikarenakan <strong><?=$row->alasan?></strong>, silahkan hubungi admin atau buat reservasi baru.
                <span class="close" data-dismiss="alert">×</span>
            </div>
        <?php
        endif;
    endforeach;
    ?>
</section>

<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?=base_url('extras/');?>plugins/morris/morris.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    </script>