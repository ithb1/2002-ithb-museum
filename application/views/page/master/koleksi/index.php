
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Koleksi
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Koleksi </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Tabel Data
          </h3>
          <div class="pull-right">
            <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalForm" onclick="clearForm()"><i class="fa fa-plus"></i> Tambah Data</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Jumlah</th>
              <th>Status</th>
              <th>Jenis</th>
              <th>action</th>
            </tr>
            </thead>
            <tbody>
              <?php 
              $no = 1;
              foreach ($rowData as $row) :
              ?>
              <tr>
                <td><?=$no++;?></td>
                <td><?=$row->nama;?></td>
                <td><?=$row->jumlah;?></td>
                <td><?=($row->status)?"Ada":"Tidak Ada";?></td>
                <td><?=$this->M_mst_jenis->getDetail($row->jenisid)->nama;?></td>
                <td>
                  <a href="" data-id="<?=$row->id?>" data-toggle="modal" data-target="#modalForm" onclick="getDetail(this)" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
                </td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Koleksi</h4>
      </div>
      <?=form_open_multipart("Master/Koleksi/add","class='form-horizontal'");
      ?>
      <div class="modal-body">

          <div class="box-body">
            <div class="row">
              <div class="col-md-12">            
                <div class="form-group">
                  <label for="nama" class="col-sm-4 control-label">Nama</label>
                  <div class="col-sm-8">
                    <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                    <input type="text" class="form-control" id="nama" placeholder="nama" name="nama" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama" class="col-sm-4 control-label">Deskripsi</label>
                  <div class="col-sm-8">
                      <textarea name="deskripsi" id="deskripsi" cols="30" rows="10" class="form-control"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama" class="col-sm-4 control-label">Jumlah</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" id="jumlah" placeholder="jumlah" name="jumlah" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama" class="col-sm-4 control-label">Jenis</label>
                  <div class="col-sm-8">
                      <select name="jenisid" id="jenisid" required class="form-control" onchange="jenis()">
                          <option value="">- jenis -</option>
                          <?php foreach($rowJenis as $row):?>
                              <option value="<?=$row->id?>" ><?=$row->nama?></option>
                          <?php endforeach;?>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama" class="col-sm-4 control-label">Status</label>
                  <div class="col-sm-8">
                      <select name="status" id="status" required class="form-control">
                          <option value="1">Ada</option>
                          <option value="0">TIdak Ada</option>
                      </select>
                  </div>
                </div>
                <div class="form-group barang" style="display: none">
                  <label for="klasifikasi" class="col-sm-4 control-label">Klasifikasi</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="klasifikasi" placeholder="klasifikasi" name="klasifikasi" value="">
                  </div>
                </div>
                <div class="form-group buku" style="display: none;">
                  <label for="kodebuku" class="col-sm-4 control-label">Kode Buku</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="kodebuku" placeholder="kodebuku" name="kodebuku" value="">
                  </div>
                </div>
                <div class="form-group buku" style="display: none;">
                  <label for="pengarang" class="col-sm-4 control-label">Pengarang</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="pengarang" placeholder="pengarang" name="pengarang" value="">
                  </div>
                </div>
                <div class="form-group buku" style="display: none;">
                  <label for="subjek" class="col-sm-4 control-label">Subjek</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="subjek" placeholder="subjek" name="subjek" value="">
                  </div>
                </div>
                <div class="form-group buku" style="display: none;">
                  <label for="bahasa" class="col-sm-4 control-label">Bahasa</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="bahasa" placeholder="bahasa" name="bahasa" value="">
                  </div>
                </div>
                <div class="form-group buku" style="display: none;">
                  <label for="ketersediaan" class="col-sm-4 control-label">Ketersediaan</label>
                  <div class="col-sm-8">
                      <input type="number" class="form-control" id="ketersediaan" placeholder="ketersediaan" name="ketersediaan" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama" class="col-sm-4 control-label">Gambar</label>
                  <div class="col-sm-8">
                    <input type="file" class="form-control" id="img" placeholder="img" name="img" value="" accept='image/*' >
                  </div>
                </div>
              </div>        
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>


<script>
  function getDetail(ini) {
    var id = $(ini).attr('data-id');
    $.ajax({
      type: 'GET',
      url: "<?=base_url('');?>Master/Koleksi/detail/"+id,
      success: function (data) {
        //Do stuff with the JSON data
          console.log(data);
         $('#id').val(id).hide();
         $('#nama').val(data.nama);
         $('#jumlah').val(data.jumlah);
         $('#deskripsi').val(data.deskripsi);
         $('#jenisid').val(data.jenisid);
         $('#status').val(data.status);
         $('#img').val(data.img);
         $('#klasifikasi').val(data.klasifikasi);
         $('#kodebuku').val(data.kodebuku);
         $('#pengarang').val(data.pengarang);
         $('#subjek').val(data.subjek);
         $('#bahasa').val(data.bahasa);
         $('#ketersediaan').val(data.ketersediaan);
        }
    });
  }

  function jenis() {
      var jenisid = $("#jenisid").val();
      if(jenisid == 1) {
          $(".barang").show();
          $(".buku").hide();
      }
      else if(jenisid == 2) {
          $(".buku").show();
          $(".barang").hide();
      }
      else{
          $(".barang").hide();
          $(".buku").hide();
      }
  }

  function clearForm() {    
     $('#id').val("");
     $('#nama').val("");
     $('#jumlah').val("");
     $('#deskripsi').val("");
     $('#jenisid').val("");
     $('#status').val("");
     $('#img').val("");
     $('#klasifikasi').val("");
     $('#kodebuku').val("");
     $('#pengarang').val("");
     $('#subjek').val("");
     $('#bahasa').val("");
     $('#ketersediaan').val("");
  }
</script>