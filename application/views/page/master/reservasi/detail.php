
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Pengunjung Acara
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Pengunjung Acara </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Pengunjung Acara <b><?=$rowAcara->nama?></b>
          </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Organisasi</th>
              <th>No Handphone</th>
              <th>Email</th>
            </tr>
            </thead>
            <tbody>
              <?php 
              $no = 1;
              foreach ($rowData as $row) :
              ?>
              <tr>
                <td><?=$no++;?></td>
                <td><?=$row->nama;?></td>
                <td><?=$row->organisasi;?></td>
                <td><?=$row->nohp;?></td>
                <td><?=$row->email;?></td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Acara</h4>
      </div>
      <?=form_open("Master/Acara/add","class='form-horizontal'");
      ?>
      <div class="modal-body">

          <div class="box-body">
            <div class="row">
              <div class="col-md-12">            
                <div class="form-group">
                  <label for="nama" class="col-sm-4 control-label">Nama</label>
                  <div class="col-sm-8">
                    <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                    <input type="text" class="form-control" id="nama" placeholder="nama" name="nama" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama" class="col-sm-4 control-label">Deskripsi</label>
                  <div class="col-sm-8">
                      <textarea name="deskripsi" id="deskripsi" cols="30" rows="10" class="form-control"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="jam" class="col-sm-4 control-label">Jam</label>
                  <div class="col-sm-8">
                    <input type="time" class="form-control" id="jam" placeholder="jam" name="jam" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="tanggal" class="col-sm-4 control-label">Tanggal</label>
                  <div class="col-sm-8">
                    <input type="date" class="form-control" id="tanggal" placeholder="tanggal" name="tanggal" value="">
                  </div>
                </div>
              </div>        
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>


<script>
  function getDetail(ini) {
      var id = $(ini).attr('data-id');
      $.ajax({
          type: 'GET',
          url: "<?=base_url('');?>Master/Acara/detail/"+id,
          success: function (data) {
              //Do stuff with the JSON data
              console.log(data);
              $('#id').val(id).hide();
              $('#nama').val(data.nama);
              $('#jam').val(data.jam);
              $('#tanggal').val(data.tanggal);
              $('#deskripsi').val(data.deskripsi);
          }
      });
  }

  function clearForm() {    
     $('#id').val("");
     $('#nama').val("");
     $('#jam').val("");
     $('#tanggal').val("");
     $('#deskripsi').val("");
  }
</script>