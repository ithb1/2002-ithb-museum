
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Tur
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Tur </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">

        <?php if($this->session->userdata("warning")):?>
        <div class="alert alert-warning" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            <?=$this->session->userdata("warning")?>
        </div>
        <?php endif; ?>

      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Tabel Data
          </h3>
          <div class="pull-right">
            <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalForm" onclick="clearForm()"><i class="fa fa-plus"></i> Tambah Data</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Member</th>
              <th>Organisasi</th>
              <th>Jumlah Pengunjung</th>
              <th>Tanggal</th>
              <th>Jam</th>
              <th>File</th>
              <th>Note</th>
              <th>Status</th>
              <th>action</th>
            </tr>
            </thead>
            <tbody>
              <?php 
              $no = 1;
              foreach ($rowData as $row) :
              ?>
              <tr>
                  <td><?=$no++;?></td>
                  <td><?=$this->M_user->getDetail($row->userid)->fullname;?></td>
                  <td><?=$this->M_user->getDetail($row->userid)->organisasi;?></td>
                  <td><?=$row->jumlahPengunjung;?></td>
                  <td><?=date("d-m-Y", strtotime($row->tanggal));?></td>
                  <td><?=date("H:i", strtotime($row->jam));?></td>
                  <td>
                      <?php if($row->file):?>
                      <a href="<?=site_url('extras/upload/attachment/'.$row->file);?>" class="btn btn-xs btn-success"><i class="fa fa-download" title="download"></i></a>
                      <?php endif;?>
                  </td>
                  <td><?=$row->note;?></td>
                  <td>
                      <?php if($row->status >= 0):?>
                          <?php if(strtotime($row->tanggal) <= strtotime(date("Y-m-d"))):?>
                            <label for="" class="label label-primary">COMPLETED</label>
                          <?php else:?>
                            <label for="" class="label label-<?=($row->status == 0)?'warning':'success';?>"><?=($row->status == 0)?'NEED APPROVE':'APPROVED';?></label>
                          <?php endif;?>
                      <?php else:?>
                          <label for="" class="label label-danger">REJECTED</label>
                      <?php endif;?>
                  </td>
                  <td>
                      <?php if($user->roleid == 1):?>
                        <?php if($row->status == 0):
                              if(strtotime($row->tanggal) >= strtotime(date("Y-m-d"))):?>
                          <a href="<?=site_url('Reservasi/approve/'.$row->id);?>" class="btn btn-xs btn-success"><i class="fa fa-check"></i></a>
                          <a href="#" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modalReject" data-id="<?=$row->id?>" onclick="getDetail(this)"><i class="fa fa-times-circle"></i></a>
                        <?php endif;
                        endif;?>
                      <?php endif;?>
                      &nbsp
                      |
                      &nbsp
                      <?php if($user->roleid == 1 || ($user->roleid == 2 && $row->status == 0)):?>
                      <a href="#" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modalNote" data-id="<?=$row->id?>" onclick="getDetail(this)"><i class="fa fa-files-o"></i></a>
                      <a href="<?=site_url('Reservasi/delete/'.$row->id);?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      <?php endif;?>
                  </td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Acara</h4>
      </div>
      <?=form_open_multipart("Reservasi/add","class='form-horizontal'");
      ?>
        <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
        <div class="modal-body">

          <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="tanggal" class="col-sm-4 control-label">Tanggal</label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" id="tanggal" placeholder="tanggal" name="tanggal" value="" min="<?=date('Y-m-d', strtotime(date('Y-m-d').'+14 days'))?>">
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="jam" class="col-sm-4 control-label">Jam</label>
                      <div class="col-sm-8">
                        <input type="time" class="form-control" id="jam" placeholder="jam" name="jam" value="" min="10:00" max="16:00">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="jumlahPengunjung" class="col-sm-4 control-label">Jumlah Pengunjung</label>
                      <div class="col-sm-8">
                        <input type="number" class="form-control" id="jumlahPengunjung" placeholder="jumlahPengunjung" name="jumlahPengunjung" value="" min="1" max="35">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="file" class="col-sm-4 control-label">Attachment</label>
                      <div class="col-sm-8">
                        <input type="file" class="form-control" id="file" placeholder="file" name="file" value="" min="1" max="35">
                      </div>
                    </div>
                </div>
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>
<div id="modalReject" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reject</h4>
      </div>
      <?=form_open("Reservasi/reject","class='form-horizontal'");
      ?>
        <input type="hidden" class="form-control" id="idReject" placeholder="id" name="id" value="">
        <div class="modal-body">

          <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="alasan" class="col-sm-4 control-label">Alasan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="alasan" placeholder="alasan" name="alasan" value="" min="<?=date('Y-m-d', strtotime(date('Y-m-d').'+14 days'))?>">
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>
<div id="modalNote" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Note</h4>
      </div>
      <?=form_open("Reservasi/note","class='form-horizontal'");
      ?>
        <input type="hidden" class="form-control" id="idNote" placeholder="id" name="id" value="">
        <div class="modal-body">

          <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="note" class="col-sm-4 control-label">Note</label>
                        <div class="col-sm-8">
                            <textarea name="note" id="note" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>


<script>
  function getDetail(ini) {
      var id = $(ini).attr('data-id');
      $.ajax({
          type: 'GET',
          url: "<?=base_url('');?>Master/Acara/detail/"+id,
          success: function (data) {
              //Do stuff with the JSON data
              console.log(data);
              $('#id').val(id).hide();
              $('#idReject').val(id).hide();
              $('#idNote').val(id).hide();
              $('#nama').val(data.nama);
              $('#jam').val(data.jam);
              $('#tanggal').val(data.tanggal);
              $('#deskripsi').val(data.deskripsi);
              $('#note').val(data.note);
          }
      });
  }

  function clearForm() {    
     $('#id').val("");
     $('#nama').val("");
     $('#jam').val("");
     $('#tanggal').val("");
     $('#deskripsi').val("");
     $('#note').val("");
  }
</script>