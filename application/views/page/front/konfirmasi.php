
<!-- Main content -->
<section class="content">
  <div class="row">
      <div class="col-xs-12">
          <div class="box">
              <div class="box-body">
                  <div class="alert alert-success alert-dismissible">
                      <h4><i class="icon fa fa-check"></i> Selamat!</h4>
                      Selamat, data anda telah masuk sistem kami.
                  </div>
              </div>
          </div>
      </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
