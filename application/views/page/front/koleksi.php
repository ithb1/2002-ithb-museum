<?php
$num_char = 200;
?>

<style>
    td{
        padding: 5px;
    }
</style>

<!-- Main content -->
<section class="content">

    <div class="alert alert-info fade in m-b-15">
        pilih salah satu kategori untuk melihat daftar koleksi
    </div>

    <h3>KOLEKSI</h3>
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php foreach ($rowJenis as $index => $jenis):?>
                        <li class=""><a href="#tab_<?=$index?>" data-toggle="tab"><?=$jenis->nama?></a></li>
                    <?php endforeach;?>
                </ul>
                <div class="tab-content">
                    <?php foreach ($rowJenis as $index => $row):
                        $rowData = $this->M_mst_koleksi->getAllBy("jenisid = $row->id");
                        ?>
                        <div class="tab-pane " id="tab_<?=$index?>">
                            <div class="row">
                                <?php foreach ($rowData as $row):
                                    $gambar = base_url('extras/upload/koleksi/').$row->img;
                                    ?>
                                    <div class="col-xs-4">
                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">
                                                    <?=$row->nama?>
                                                </h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table>
                                                    <tr>
                                                        <td colspan="2"><img src="<?=$gambar;?>" style="height: 200px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jumlah </td>
                                                        <td>: <?=$row->jumlah ?></td>
                                                    </tr>
                                                    <?php if($row->jenisid == '1'):?>
                                                    <tr>
                                                        <td>Klasisfikasi </td>
                                                        <td>: <?=$row->klasifikasi ?></td>
                                                    </tr>
                                                    <?php else:?>
                                                        <tr>
                                                            <td>Kode Buku </td>
                                                            <td>: <?=$row->kodebuku ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Pengarang </td>
                                                            <td>: <?=$row->pengarang ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Subjek </td>
                                                            <td>: <?=$row->subjek ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bahasa </td>
                                                            <td>: <?=$row->bahasa ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ketersediaan </td>
                                                            <td>: <?=$row->ketersediaan ?></td>
                                                        </tr>
                                                    <?php endif;?>
                                                    <tr>
                                                        <td colspan="2"><?=substr($row->deskripsi, 0, $num_char) . '...';?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <a href="" data-toggle="modal" data-nama="<?=$row->nama?>" data-jumlah="<?=$row->jumlah?>" data-deskripsi="<?=$row->deskripsi?>" data-gambar="<?=$gambar?>" data-target="#modalDetail" onclick="getDetail(this)" class="btn btn-xs btn-primary">read more</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                <?php endforeach;?>
                                <!-- /.col -->
                            </div>
                        </div>
                    <?php endforeach;?>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
        <!-- /.col -->
    </div>

    <!-- /.row -->
</section>
<!-- /.content -->


<!-- Modal -->
<div id="modalDetail" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="nama"></h4>Jumlah <small id="jumlah"></small>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="" id="gambar" style="width: 100%;">
                        </div>
                        <div class="col-md-6">
                            <div id="deskripsi"></div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<script>
    function getDetail(ini) {
        clearForm();
        var nama = $(ini).attr('data-nama');
        var jumlah = $(ini).attr('data-jumlah');
        var deskripsi = $(ini).attr('data-deskripsi');
        var gambar = $(ini).attr('data-gambar');
        $('#nama').append(nama);
        $('#jumlah').append(jumlah);
        $('#deskripsi').append(deskripsi);
        $("#gambar").attr("src",gambar);
    }

    function clearForm() {
        $('#nama').empty();
        $('#jumlah').empty();
        $('#deskripsi').empty();
        $("#gambar").attr("src","https://os.mbed.com/media/components/pinouts/not_available.jpg");
    }
</script>