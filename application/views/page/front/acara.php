<style>
    td{
        padding: 5px;
    }
</style>

<!-- Main content -->
<section class="content">
    <h3>ACARA</h3>
  <div class="row">
      <?php foreach ($rowData as $row):?>
    <div class="col-xs-3">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            <?=$row->nama?>
          </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="height: 150px">
            <table>
                <tr>
                    <td>Jam </td>
                    <td>: <?=date("H:i", strtotime($row->jam)) ?></td>
                </tr>
                <tr>
                    <td>Tanggal </td>
                    <td>: <?=date("d-m-Y", strtotime($row->tanggal)) ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?=$row->deskripsi?></td>
                </tr>
            </table>
        </div>
          <div class="box-footer">
              <a href="" data-id="<?=$row->id?>" data-toggle="modal" data-target="#modalForm" onclick="getDetail(this)" class="btn btn-xs btn-primary">reservasi</a>
          </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
      <?php endforeach;?>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->


<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reservasi</h4>
            </div>
            <?=form_open("FrontPage/reservasi","class='form-horizontal'");
            ?>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <input type="hidden" class="form-control" id="acaraid" placeholder="acaraid" name="acaraid" value="">
                                    <input type="text" class="form-control" id="nama" placeholder="nama" name="nama" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nohp" class="col-sm-4 control-label">No Handphone</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nohp" placeholder="nohp" name="nohp" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="email" placeholder="email" name="email" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>



<script>
    function getDetail(ini) {
        clearForm();
        var id = $(ini).attr('data-id');
        $('#acaraid').val(id).hide();
    }

    function clearForm() {
        $('#acaraid').val("");
        $('#nama').val("");
        $('#nohp').val("");
        $('#email').val("");
    }
</script>