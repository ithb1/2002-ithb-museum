<?php
    $iduser = $this->session->userdata("id");
    $user = $this->M_user->getDetail($iduser);
?>
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>

    <li class="treeview <?=($this->uri->segment(2) == 'Jenis' || $this->uri->segment(2) == 'Status' || $this->uri->segment(2) == 'User')?'active':''?>">
        <a href="#">
          <i class="fa fa-gear"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li class="<?=($this->uri->segment(2) == 'User')?'active':''?>"><a href="<?=site_url('Master/User')?>"><span>User</span></a></li>
        </ul>
    </li>
    <li class="<?=($this->uri->segment(2) == 'Acara')?'active':''?>"><a href="<?=site_url('Master/Acara')?>"><i class="fa fa-calendar"></i> <span>Acara</span></a></li>
    <li class="<?=($this->uri->segment(2) == 'Artikel')?'active':''?>"><a href="<?=site_url('Master/Artikel')?>"><i class="fa fa-newspaper-o"></i> <span>Artikel</span></a></li>
    <li class="<?=($this->uri->segment(2) == 'Koleksi')?'active':''?>"><a href="<?=site_url('Master/Koleksi')?>"><i class="fa fa-archive"></i> <span>Koleksi</span></a></li>
    <li class="<?=($this->uri->segment(1) == 'Reservasi')?'active':''?>"><a href="<?=site_url('Reservasi')?>"><i class="fa fa-bookmark"></i> <span>Reservasi</span></a></li>
</ul>