<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservasi extends CI_Controller {
	
	var $kelas = "Reservasi";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}

        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index(){
	    if($this->user->roleid == 1)
		    $data["rowData"] = $this->M_reservasi_tur->getAll();
        else
            $data["rowData"] = $this->M_reservasi_tur->getAllBy("userid = ".$this->user->userid);

        $data['user'] = $this->user;
        $data['konten'] = "master/reservasi/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_reservasi_tur->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
        $data["tanggal"] = $tanggal = $this->input->post("tanggal");
        $data["jam"] = $this->input->post("jam");
        $data["jumlahPengunjung"] = $jumlahPengunjung = $this->input->post("jumlahPengunjung");

        if ($_FILES['file']['name']) {
            $uploadPath = "extras/upload/attachment/";
            $filename = $this->unggah->upload_files('file', "", $uploadPath);
            $data["file"] = str_replace(" ", "_", $filename);
        }

        if($id)
            $this->M_reservasi_tur->update($id,$data);
        else {
//            cek maksimal 2 approved
            $rowData = $this->M_reservasi_tur->getAllBy("tanggal = '$tanggal' AND status = 1" );
            if(count($rowData) < 2){
                if($jumlahPengunjung <= 35){
                    $this->M_reservasi_tur->update($id,$data);
                    $data["status"] = 0;
                    $data["userid"] = $this->user->userid;
                    $this->M_reservasi_tur->add($data);
                    $this->session->set_flashdata("success","jadwal berhasil ditambahkan ");
                }
                else{
                    $this->session->set_flashdata("warning","jumlah pengunjung melebihi batas kuota maksimal pengunjung yaitu 35 orang");
                }
            }else{
                $this->session->set_flashdata("warning","jadwal tur sudah penuh pada tanggal ".date("d M Y", strtotime($tanggal)));
            }
        }

		redirect($this->kelas);
	}

    public function approve($id){
        $data["status"] = 1;
        $this->M_reservasi_tur->update($id,$data);
        redirect($this->kelas);
    }

    public function reject(){
		$id = $this->input->post("id");
        $data["status"] = -1;
        $data["alasan"] = $this->input->post("alasan");

        $this->M_reservasi_tur->update($id,$data);
		redirect($this->kelas);
	}

    public function note(){
		$id = $this->input->post("id");
        $data["note"] = $this->input->post("note");

        $this->M_reservasi_tur->update($id,$data);
		redirect($this->kelas);
	}

	public function delete($id){		
		$this->M_reservasi_tur->delete($id);
		redirect($this->kelas);
	}
}
