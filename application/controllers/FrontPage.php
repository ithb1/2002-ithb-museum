<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FrontPage extends CI_Controller {

    var $kelas = "FrontPage";

	function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$data["konten"] = "front/index";
		$this->load->view('templateFront',$data);
	}

	public function acara()
	{
        $data["rowData"] = $this->M_mst_acara->getAll();
		$data["konten"] = "front/acara";
		$this->load->view('templateFront',$data);
	}

	public function artikel()
	{
        $data["rowJenis"] = $this->M_mst_jenis->getAllBy("grup = 'artikel'");
        $data["rowData"] = $this->M_mst_artikel->getAll();
		$data["konten"] = "front/artikel";
		$this->load->view('templateFront',$data);
	}

	public function koleksi()
	{
        $data["rowJenis"] = $this->M_mst_jenis->getAllBy("grup = 'koleksi'");
        $data["rowData"] = $this->M_mst_koleksi->getAll();
		$data["konten"] = "front/koleksi";
		$this->load->view('templateFront',$data);
	}

	public function reservasi()
	{
        $data["acaraid"] = $this->input->post("acaraid");
        $data["nama"] = $this->input->post("nama");
        $data["nohp"] = $this->input->post("nohp");
        $data["email"] = $this->input->post("email");
        $this->M_reservasi_acara->add($data);

		$data["konten"] = "front/konfirmasi";
		$this->load->view('templateFront',$data);
	}

	public function register()
	{
        $data["username"] = $this->input->post("username");
        $data["fullname"] = $this->input->post("fullname");
        $data["email"] = $this->input->post("email");
        $data["password"] = $this->encrypt->encode($this->input->post("password"));
        $data["roleid"] = 2;
        $data["organisasi"] = $this->input->post("organisasi");
        $this->M_user->add($data);

		$data["konten"] = "front/konfirmasi";
		$this->load->view('templateFront',$data);
	}
}
