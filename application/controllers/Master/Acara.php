<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acara extends CI_Controller {
	
	var $kelas = "Master/Acara";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}

        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index(){
		$data["rowData"] = $this->M_mst_acara->getAll();
		$data['konten'] = "master/acara/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_mst_acara->getDetail($id);
	    echo json_encode( $rowData );
	}

    public function detailPengunjung($acaraid){
        $data["rowAcara"] = $this->M_mst_acara->getDetail($acaraid);
        $data["rowData"] = $this->M_reservasi_acara->getAllBy("acaraid = $acaraid");
        $data['konten'] = "master/acara/detail";
        $this->load->view('template',$data);
    }

	public function add(){
		$id = $this->input->post("id");
		$data["nama"] = $this->input->post("nama");
		$data["jam"] = $this->input->post("jam");
		$data["tanggal"] = $this->input->post("tanggal");
		$data["deskripsi"] = $this->input->post("deskripsi");
        $data["userid"] = $this->user->userid;

		if($id) 
			$this->M_mst_acara->update($id,$data);
		else 
			$this->M_mst_acara->add($data);

		redirect($this->kelas);
	}

	public function delete($id){		
		$this->M_mst_acara->delete($id);
		redirect($this->kelas);
	}
}
