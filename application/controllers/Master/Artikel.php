<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {
	
	var $kelas = "Master/Artikel";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}

        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index(){
		$data["rowData"] = $this->M_mst_artikel->getAll();
        $data["rowJenis"] = $this->M_mst_jenis->getAllBy("grup = 'artikel'");
		$data['konten'] = "master/artikel/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_mst_artikel->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
        $id = $this->input->post("id");
        $data["nama"] = $this->input->post("nama");
        $data["nama_mandarin"] = utf8_encode($this->input->post("nama_mandarin"));
        $data["lokasi"] = $this->input->post("lokasi");
        $data["deskripsi"] = $this->input->post("deskripsi");
        $data["jenisid"] = $this->input->post("jenisid");
        $data["img"] = $this->input->post("img");
        $data["userid"] = $this->user->userid;

        if($id)
            $this->M_mst_artikel->update($id,$data);
        else
            $this->M_mst_artikel->add($data);

        $docid = ($id != "") ? $id : $this->M_mst_artikel->getMax("id");

        $this->session->set_flashdata("success","Data Berhasil disimpan");
        if ($_FILES['img']['name']) {
            $uploadPath = "extras/upload/artikel/";
            $filename = $this->unggah->upload_files('img', "", $uploadPath);
            $dataUpdate = array(
                "img" => str_replace(" ", "_", $filename)
            );
            $this->M_mst_artikel->update($docid,$dataUpdate);

        } else if(!$id){
//            $this->session->set_flashdata("warning","File gagal disimpan");
//            $this->session->unset_flashdata("success");
            $this->M_mst_artikel->delete($docid);
        }

        redirect($this->kelas);
	}

	public function delete($id){		
		$this->M_mst_artikel->delete($id);
		redirect($this->kelas);
	}
}
