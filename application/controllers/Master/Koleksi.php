<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Koleksi extends CI_Controller {
	
	var $kelas = "Master/Koleksi";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}

        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index(){
		$data["rowData"] = $this->M_mst_koleksi->getAll();
		$data["rowJenis"] = $this->M_mst_jenis->getAllBy("grup = 'koleksi'");
		$data['konten'] = "master/koleksi/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_mst_koleksi->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["nama"] = $this->input->post("nama");
		$data["jumlah"] = $this->input->post("jumlah");
		$data["deskripsi"] = $this->input->post("deskripsi");
		$data["jenisid"] = $this->input->post("jenisid");
		$data["status"] = $this->input->post("status");
		$data["img"] = $this->input->post("img");
        $data["klasifikasi"] = $this->input->post("klasifikasi");
        $data["kodebuku"] = $this->input->post("kodebuku");
        $data["pengarang"] = $this->input->post("pengarang");
        $data["subjek"] = $this->input->post("subjek");
        $data["bahasa"] = $this->input->post("bahasa");
        $data["ketersediaan"] = $this->input->post("ketersediaan");
        $data["userid"] = $this->user->userid;

		if($id) 
			$this->M_mst_koleksi->update($id,$data);
		else 
			$this->M_mst_koleksi->add($data);

        $docid = ($id != "") ? $id : $this->M_mst_koleksi->getMax("id");

        $this->session->set_flashdata("success","Data Berhasil disimpan");
        if ($_FILES['img']['name']) {
            $uploadPath = "extras/upload/koleksi/";
            $filename = $this->unggah->upload_files('img', "", $uploadPath);
            $dataUpdate = array(
                "img" => str_replace(" ", "_", $filename)
            );
            $this->M_mst_koleksi->update($docid,$dataUpdate);

        } else if(!$id){
//            $this->session->set_flashdata("warning","File gagal disimpan");
//            $this->session->unset_flashdata("success");
            $this->M_mst_koleksi->delete($docid);
        }

		redirect($this->kelas);
	}

	public function delete($id){		
		$this->M_mst_koleksi->delete($id);
		redirect($this->kelas);
	}
}
